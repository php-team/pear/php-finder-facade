php-finder-facade (2.0.0-1) experimental; urgency=medium

  * Upload incompatible version with phploc and phpcpd to experimental

  [ David Prévot ]
  * Move to namespaced phpunit versions
  * Simplify test suite handling

  [ Sebastian Bergmann ]
  * Prepare release

 -- David Prévot <taffit@debian.org>  Sat, 08 Feb 2020 14:41:33 -1000

php-finder-facade (1.2.3-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Bump copyright year

  [ David Prévot ]
  * debian/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update standards version to 4.5.0
  * debian/copyright: Update copyright (years)
  * debian/upstream/metadata:
    + Set fields: Bug-Database, Bug-Submit, Repository, Repository-Browse
    + Remove obsolete fields: Contact, Name

 -- David Prévot <taffit@debian.org>  Sat, 08 Feb 2020 14:30:10 -1000

php-finder-facade (1.2.2-3) unstable; urgency=medium

  * d/control:
    - Move repository to salsa
    - Update Standards-Version to 4.4.0
    - Bump debhelper from old 9 to 12.
    - Marck php-finder-facade-doc as Multi-Arch: foreign
    - Use fonts-liberation instead of embedded font
  * d/rules:
    - Drop get-orig-source target
    - Use debian/clean for directories
  * d/copyright: Use versioned copyright format URI.
  * d/u/metadata: Set upstream metadata fields: Contact, Name.
  * Compatibility with recent PHPUnit (8)

 -- David Prévot <taffit@debian.org>  Fri, 09 Aug 2019 15:43:36 -1000

php-finder-facade (1.2.2-2) unstable; urgency=medium

  * Move to namespaced phpunit versions (Closes: #882900)

 -- David Prévot <taffit@debian.org>  Mon, 27 Nov 2017 13:52:23 -1000

php-finder-facade (1.2.2-1) unstable; urgency=medium

  * Drop versioned dependency satisfied in stable
  * Update Standards-Version to 4.1.1

 -- David Prévot <taffit@debian.org>  Sun, 19 Nov 2017 15:22:09 -1000

php-finder-facade (1.2.1-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Bump copyright year

  [ David Prévot ]
  * Update copyright (years)
  * Update Standards-Version to 3.9.7
  * Build with recent pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Tue, 08 Mar 2016 21:14:18 -0400

php-finder-facade (1.2.0-1) unstable; urgency=medium

  * Update copyright (year)
  * Restore previous build files
  * Adapt build to current source

 -- David Prévot <taffit@debian.org>  Sun, 07 Jun 2015 21:00:43 -0400

php-finder-facade (1.1.0+comp-1) unstable; urgency=medium

  * Adapt packaging to Composer source
  * Bump standards version to 3.9.6
  * Provide API documentation
  * Provide DEP-8 tests
  * Depend on recent php-symfony-finder

 -- David Prévot <taffit@debian.org>  Tue, 14 Apr 2015 15:18:54 -0400

php-finder-facade (1.1.0-1) unstable; urgency=low

  * Initial Release (Closes: #746296)

 -- David Prévot <taffit@debian.org>  Mon, 28 Apr 2014 19:18:24 -0400
